<?php

/**
 * @file
 * uw_blog_comments.features.inc
 */

/**
 * Implements hook_user_default_permissions_alter().
 */
function uw_blog_comments_user_default_permissions_alter(&$data) {
  if (isset($data['access comments'])) {
    $data['access comments']['roles'] = array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'site manager' => 'site manager',
      'uWaterloo faculty' => 'uWaterloo faculty',
      'uWaterloo staff' => 'uWaterloo staff',
      'uWaterloo student' => 'uWaterloo student',
    ); /* WAS: '' */
  }
  if (isset($data['administer comments'])) {
    $data['administer comments']['roles'] = array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ); /* WAS: '' */
  }
  if (isset($data['edit own comments'])) {
    $data['edit own comments']['roles'] = array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ); /* WAS: '' */
  }
  if (isset($data['post comments'])) {
    $data['post comments']['roles'] = array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ); /* WAS: '' */
  }

  if (isset($data['use text format uw_tf_basic'])) {
    $data['use text format uw_tf_basic']['roles']['authenticated user'] = 'authenticated user'; /* WAS: '' */
  }
}

/**
 * Implements hook_strongarm_alter().
 */
function uw_blog_comments_strongarm_alter(&$data) {
  if (isset($data['comment_uw_blog'])) {
    $data['comment_uw_blog']->value = 2; /* WAS: 1 */
  }
  if (isset($data['forward_display_uw_blog'])) {
    $data['forward_display_uw_blog']->value = 1; /* WAS: TRUE */
  }
}
