<?php

/**
 * @file
 * uw_blog_comments.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_blog_comments_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: user_permission.
  $overrides["user_permission.access comments.roles"] = array(
    'administrator' => 'administrator',
    'authenticated user' => 'authenticated user',
    'site manager' => 'site manager',
    'uWaterloo faculty' => 'uWaterloo faculty',
    'uWaterloo staff' => 'uWaterloo staff',
    'uWaterloo student' => 'uWaterloo student',
  );
  $overrides["user_permission.administer comments.roles"] = array(
    'administrator' => 'administrator',
    'site manager' => 'site manager',
  );
  $overrides["user_permission.edit own comments.roles"] = array(
    'administrator' => 'administrator',
    'authenticated user' => 'authenticated user',
    'content author' => 'content author',
    'content editor' => 'content editor',
    'site manager' => 'site manager',
  );
  $overrides["user_permission.post comments.roles"] = array(
    'administrator' => 'administrator',
    'authenticated user' => 'authenticated user',
    'content author' => 'content author',
    'content editor' => 'content editor',
    'site manager' => 'site manager',
  );
  $overrides["user_permission.use text format uw_tf_basic.roles|authenticated user"] = 'authenticated user';

  // Exported overrides for: variable.
  $overrides["variable.comment_uw_blog.value"] = 2;
  $overrides["variable.forward_display_uw_blog.value"] = 1;

  return $overrides;
}
